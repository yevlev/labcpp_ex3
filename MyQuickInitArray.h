/**
 * MyQuickInitArray.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This template class represents a generic array, which initialization
 *          takes constant time, and so the access to any element j.
 *
 * Methods:  MyQuickInitArray()    - Constructor.
 *									 Gets the size of the array.
 *                                   throws exception if failed to allocate memory.
 *
 *			 ~MyQuickInitArray()   - Destructor.
 *
 *			 TODO
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef MY_QUICK_INIT_ARRAY_H
#define MY_QUICK_INIT_ARRAY_H

#include <iostream>

#define OUT_OF_MEM_ERR_MSG "Error: memory allocation failed"
#define OUT_OF_RANGE_ERR_MSG "Error: array index out of range"


template <class T>
class MyQuickInitArray
{

public:

	/**
	* Constructor.
	* Gets the size of the array.
	* throws exception if failed to allocate memory.
	*/
	MyQuickInitArray(int arrSize) : _arrSize(arrSize), _initElementsCounter(0), t_defVal()
	{
		if (allocateObjMemory() == false)  //memory alloc. for arrays failed.
		{
			std::cerr << OUT_OF_MEM_ERR_MSG << " during construction." << std::endl;
			throw std::exception(OUT_OF_MEM_ERR_MSG);
		}
	}


	/**
	* Copy Constructor.
	* Gets other array object.
	* throws exception if failed to allocate memory.
	*/
	MyQuickInitArray(const MyQuickInitArray<T>& t_other) : _arrSize(t_other.getSize()),
					_initElementsCounter(t_other._initElementsCounter), t_defVal(t_other.getDefVal())
	{

		if (allocateObjMemory() == false) //memory alloc. for arrays failed.
		{
			std::cerr << OUT_OF_MEM_ERR_MSG << " during construction." << std::endl;
			throw std::exception(OUT_OF_MEM_ERR_MSG);
		}

		//copying from other arrays to arrays of this.
		copyArrays(t_other);
	}

	/**
	* assignment operator.
	* Gets other array object.
	* throws exception if failed to allocate memory.
	*/
	const MyQuickInitArray<T>& operator=(const MyQuickInitArray<T>& t_other)
	{
		//freeing the current memory
		if (this != &t_other) //to forbid a = a.
		{
			//free curr. memory.
			freeObjMemory();

			_arrSize = t_other.getSize();
			_initElementsCounter = t_other._initElementsCounter;

			//allocate new memory.
			if (allocateObjMemory() == false) //memory alloc. for arrays failed.
			{
				std::cerr << OUT_OF_MEM_ERR_MSG << " during assignment." << std::endl;
				throw std::exception(OUT_OF_MEM_ERR_MSG);
			}
			
			//copying from other arrays to arrays of this.
			copyArrays(t_other);
		}

		return *this;
	}


	/**
	* returns the size of this array.
	*
	*/
	int getSize() const
	{
		return _arrSize;
	}

	/**
	* returns reference to default value (object) of type T.
	*
	*/
	const T& getDefVal() const
	{
		return t_defVal;
	}


	/**
	* non-const overload of subscript operator. 
	* returns reference to object at ind. index. 
	* if index is out of bounds, out_of_range exception is thrown.
	* also, throws exception if failed to allocate memory.
	*/
	T& operator[](int index)
	{
		if ((index < 0) || (index >= _arrSize))  //out of bounds.
		{
			freeObjMemory();
			std::cerr << OUT_OF_RANGE_ERR_MSG << std::endl;
			throw std::out_of_range(OUT_OF_RANGE_ERR_MSG);
		}

		bool isInitialized = true;
		int initHolderVal = _initHolder[index];
		
		if ((initHolderVal < 0) || (initHolderVal >= _initElementsCounter)) 
		{
			//not in _initIndexes range. 
			isInitialized = false;
		}
		else
		{
			if (_initIndexes[initHolderVal] != index)
			{
				//value in _initIndexes don't match.
				isInitialized = false;
			}
		}

		if (!isInitialized)  //init. element at index.
		{
			t_ptrArray[index] = new T();
			_initIndexes[_initElementsCounter] = index;
			_initHolder[index] = _initElementsCounter;
			_initElementsCounter++;
		}
			
		return (*t_ptrArray[index]);
	}

	/**
	* const overload of subscript operator. 
	* returns const reference to object at ind. index. 
	* if index is out of bounds, out_of_range exception is thrown.
	*/
	const T& operator[](int index) const
	{
		if ((index < 0) || (index >= _arrSize)) //out of bounds.
		{
			freeObjMemory();
			std::cerr << OUT_OF_RANGE_ERR_MSG << std::endl;
			throw std::out_of_range(OUT_OF_RANGE_ERR_MSG);
		}

		bool isInitialized = true;
		int initHolderVal = _initHolder[index];
		
		if ((initHolderVal < 0) || (initHolderVal > _initElementsCounter))
		{
			//not in _initIndexes range. 
			isInitialized = false;
		}
		else
		{
			if (_initIndexes[initHolderVal] != index)
			{
				//value in _initIndexes don't match.
				isInitialized = false;
			}
		}

		if (!isInitialized)
		{
			return t_defVal;  //return default value.
		}
			
		return (*t_ptrArray[index]);
	}
	

	/**
	* Destructor.
	*/
	~MyQuickInitArray()
	{
		freeObjMemory();
	}


private:

	/*
	* allocates memory for the 3 array data members (when _arrSize already init.).
	* if one of allocations fails, frees already allocated memory,
	* and returns false. 
	* otherwise - return true;
	* 
	*/
	bool allocateObjMemory()
	{
		//allocates memory for the main array -  of pointers to elements.
		t_ptrArray = (T**)malloc(_arrSize * sizeof(T*));

		if (t_ptrArray == NULL)
		{
			return false;
		}

		//allocates memory for array of initialized indexes.
		_initIndexes = (int*)malloc(_arrSize * sizeof(int));
		
		if (_initIndexes == NULL)
		{
			free(t_ptrArray);

			return false;
		}

		//allocates memory for array that holds info. to know if element at index j was init.
		_initHolder = (int*)malloc(_arrSize * sizeof(int));
		
		if (_initHolder == NULL)
		{
			free(t_ptrArray);
			free(_initIndexes);

			return false;
		}

		return true;
	}


	/*
	* frees all memory of this object.
	* 
	*/
	void freeObjMemory()
	{
		for (int i = 0; i < _initElementsCounter; i++)
		{
			//deleting only initialized indexes.
			delete (t_ptrArray[_initIndexes[i]]);
		}

		//freeing arrays.
		free(t_ptrArray);
		free(_initIndexes);
		free(_initHolder);
	}


	/*
	* copyies from other arrays to arrays of this.
	* (assumes _arrSize and _initElementsCounter are already init.).
	* throws exception if failed to allocate memory.
	*/
	void copyArrays(const MyQuickInitArray<T>& t_other)
	{
		for (int i = 0; i < _arrSize; i++)
		{
			_initIndexes[i] = t_other._initIndexes[i];
			_initHolder[i] = t_other._initHolder[i];
		}

		//going only over init. indexes of other (over _initIndexes array).
		for (int i = 0; i < _initElementsCounter; i++)
		{
			//allocate new memory for every init. element.
			t_ptrArray[_initIndexes[i]] = new T(t_other[_initIndexes[i]]); //using op. [] of other.
		}
	}


	int _arrSize;
	int _initElementsCounter;
	const T t_defVal;

	T** t_ptrArray;
	int* _initIndexes;
	int* _initHolder;
	
};


#endif
